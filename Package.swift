// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PromethistSDK-iOS",
    products: [
        .library(
            name: "PromethistSDK-iOS",
            targets: ["PromethistSDK-iOS"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "PromethistSDK-iOS",
            dependencies: [
                .target(name: "PromethistSDK"),
                .target(name: "PromethistCommon"),
                .target(name: "MobileVLCKit")
            ]
        ),
        .binaryTarget(
            name: "PromethistSDK",
            path: "PromethistSDK.xcframework"
        ),
        .binaryTarget(
            name: "MobileVLCKit",
            path: "MobileVLCKit.xcframework"
        ),
        .binaryTarget(
            name: "PromethistCommon",
            path: "shared.xcframework"
        )
    ]
)
